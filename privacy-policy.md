# Privacy Policy

If enabled, NotificationLog saves your system notifications at your device.
Your notifications are not sent anywhere.

NotificationLog does not do its own encryption. Your data is encrypted if
the encryption of your device was enabled.

NotificationLog allows deleting notifications automatically after some time
and filtering which notifications are saved at all.

NotificationLog allows you to export your notifications. It is your decision
where you save the export.
