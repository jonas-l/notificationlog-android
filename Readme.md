NotificationLog is an Android App which logs the notifications (locally) and allows to view/ export all of them or all of one App.

## Screenshots

![screenshot showing the list of Apps in NotificationLog](./app/src/main/play/listings/en-US/graphics/phone-screenshots/391911807240794485.png)

![screenshot showing the notifications of one App in NotificationLog](./app/src/main/play/listings/en-US/graphics/phone-screenshots/2565105775754909137.png)

## Download

- <https://f-droid.org/de/packages/de.jl.notificationlog/>
